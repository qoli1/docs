---
---

# Bitizen Wallet 介绍

Bitizen Wallet 是一款基于安全多方计算（MPC）的门限签名方案（TSS）构建的「无私钥」、「无助记词」的非托管式多链 Web3 钱包。

{% hint style="success" %}
**Keywords**

无私钥（Keyless）

无助记词（Seedless）

非托管式（Trustless & Permissionless & Censorship-resistance）

多链（Multi-chain）
{% endhint %}
