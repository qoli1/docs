---
title: 'Offchain 钱包（中心化）'
metaTitle: 'Offchain 钱包（中心化）'
metaDescription: 'Offchain 钱包（中心化）'
---

# Offchain 钱包（中心化）

Offchain 钱包即中心化钱包，也称之为 Custodial 托管式钱包，即将钱包的私钥（资产绝对控制权）托管给某个中心化的机构（例如交易所），而用户不直接掌握控制权。

Offchain 钱包是完全中心化的 Web2 产品，因此用户体验完全和 Web2 产品一样简单易用，但掌握资产绝对控制权的中心化机构有能力挪用用户的资产，也可以冻结用户的资产。

{% hint style="success" %}
简单易用
{% endhint %}

{% hint style="danger" %}
资产有安全风险
{% endhint %}

{% hint style="danger" %}
不可以使用 Web3 应用
{% endhint %}
